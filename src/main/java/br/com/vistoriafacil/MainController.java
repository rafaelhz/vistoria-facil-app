package br.com.vistoriafacil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by rafael on 11/05/15.
 */
@Controller
@EnableAutoConfiguration
public class MainController {

    @RequestMapping("/home")
    String home() {
        return "home";
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(MainController.class, args);
    }
}
