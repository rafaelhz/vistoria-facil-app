'use strict';

/**
 * @ngdoc service
 * @name vistoriaFacilApp.UsuarioService
 * @description
 * # UsuarioService
 * Service in the vistoriaFacilApp.
 */
angular.module('vistoriaFacilWebApp')
  .factory('Usuario', function ($resource) {

    return $resource($SERVER + '/ws/usuario/:id');

  });

