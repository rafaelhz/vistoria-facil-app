'use strict';


angular.module('vistoriaFacilWebApp')

.factory('authHttpRequestInterceptor', ['$rootScope', '$injector', function ($rootScope, $injector) {
    var authHttpRequestInterceptor = {
        request: function($request) {
            angular.element('#loading').show();
            $request.headers = {authorization : "Basic " + sessionStorage.token, "Content-Type" : "application/json"}

            return $request;
        },
        response : function(response) {
          angular.element('#loading').hide();
          return response || $q.when(response);
        },
        responseError: function(response) {
          angular.element('#loading').hide();

          if(response.status != 401) {
            alert("Erro ao acessar servidor. Contacte o administrador do sistema.");
          }

          return $q.reject(response);
        }
     };

    return authHttpRequestInterceptor;
}])

.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('authHttpRequestInterceptor');
}]);
