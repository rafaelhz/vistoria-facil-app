'use strict';

/**
 * @ngdoc service
 * @name vistoriaFacilApp.ImobiliariaService
 * @description
 * # ImobiliariaService
 * Service in the vistoriaFacilApp.
 */
angular.module('vistoriaFacilWebApp')
  .factory('Imobiliaria', function ($resource) {
    var imobiliaria = $resource($SERVER + '/ws/imobiliaria/:id', {
      query: {
        method: 'GET',
        isArray: false
      }
    });

    return imobiliaria;

  });

