'use strict';

/**
 * @ngdoc service
 * @name vistoriaFacilApp.CidadeService
 * @description
 * # CidadeService
 * Service in the vistoriaFacilApp.
 */
angular.module('vistoriaFacilWebApp')
  .factory('Cidade', function ($resource) {

    return $resource($SERVER + '/cidade/:id');

  });

