'use strict';

/**
 * @ngdoc service
 * @name vistoriaFacilApp.authService
 * @description
 * # authService
 * Service in the vistoriaFacilApp.
 */

angular.module("vistoriaFacilWebApp").factory('authService',
  function ($rootScope) {

    var authorize = function (permissao) {
      var user = sessionStorage.usuario;

      if(user === undefined) {
        return false;
      }

      if(user.permissoes.indexOf(permissao) > -1) {
        return true;
      }

      return false;
    };

    return {
      authorize: authorize
    };
  });
