'use strict';

/**
 * @ngdoc service
 * @name vistoriaFacilApp.PermissaoService
 * @description
 * # PermissaoService
 * Service in the vistoriaFacilApp.
 */
angular.module('vistoriaFacilWebApp')
  .factory('Permissao', function ($resource) {

    return $resource($SERVER + '/ws/permissao/:id');

  });

