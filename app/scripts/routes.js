'use strict';

/**
 * @ngdoc overview
 * @name vistoriaFacilWebApp
 * @description
 * # vistoriaFacilWebApp
 *
 * Main module of the application.
 */

  $app.config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        requireLogin: false
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        requireLogin: true,
        permissao: "USUARIO"
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })
      .when('/imobiliaria', {
        templateUrl: 'views/imobiliaria.html',
        controller: 'ImobiliariaCtrl',
        requireLogin: true,
        permissao: "USUARIO"
      })
      .when('/imobiliaria/novo', {
        templateUrl: 'views/imobiliariaCadastro.html',
        controller: 'ImobiliariaCadastroCtrl',
        requireLogin: true,
        permissao: "USUARIO"
      })
      .when('/imobiliaria/:id', {
        templateUrl: 'views/imobiliariaCadastro.html',
        controller: 'ImobiliariaCadastroCtrl',
        requireLogin: true,
        permissao: "USUARIO"
      })
      .when('/usuario', {
        templateUrl: 'views/usuario.html',
        controller: 'UsuarioCtrl',
        requireLogin: true,
        permissao: "USUARIO"
      })
      .when('/usuario/novo', {
        templateUrl: 'views/usuarioCadastro.html',
        controller: 'UsuarioCadastroCtrl',
        requireLogin: true,
        permissao: "USUARIO"
      })
      .when('/usuario/:id', {
        templateUrl: 'views/usuarioCadastro.html',
        controller: 'UsuarioCadastroCtrl',
        requireLogin: true,
        permissao: "USUARIO"
      })
      .otherwise({
        redirectTo: '/'
      });
  })



