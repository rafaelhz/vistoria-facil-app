'use strict';

/**
 * @ngdoc overview
 * @name vistoriaFacilWebApp
 * @description
 * # vistoriaFacilWebApp
 *
 * Main module of the application.
 */

//var $SERVER = "http://localhost:8080/";
var $SERVER = "https://vistoria-facil.herokuapp.com/";

var $app = angular
  .module('vistoriaFacilWebApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'angular-flash.service',
    'angular-flash.flash-alert-directive',
    'blockUI',
    'trNgGrid'
  ]);


$app.run(['$rootScope', '$location', 'authService',
  function ($rootScope, $location, authService) {

    /* Verifica se o usuario possui permissao pagina */
    $rootScope.$on('$routeChangeStart', function (event, next, current) {

      if (next && next.$$route && next.$$route.requireLogin) {

        if (!authService.authorize(next.$$route.permissao)) {
          $rootScope.$evalAsync(function () {
            $location.path('/login');
          });
        }
      }

    });

  }]);


