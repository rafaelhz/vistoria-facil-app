'use strict';

/**
 * @ngdoc function
 * @name vistoriaFacilApp.controller:UsuariocadastroCtrl
 * @description
 * # UsuariocadastroCtrl
 * Controller of the vistoriaFacilApp
 */
angular.module('vistoriaFacilWebApp')
  .controller('UsuarioCadastroCtrl', function ($scope, Usuario, Permissao, $routeParams, $location, flash) {

    $scope.inicializar = function(){
        if ($routeParams.id != null) {
            $scope.editar();

        } else {
            $scope.novo();
        }

        $scope.permissoes = Permissao.query();
    }

    $scope.editar = function(){
        Usuario.get({ id: $routeParams.id }, function(data) {
            $scope.usuario = data;
        });
    }

    $scope.novo = function(){
        $scope.usuario = {};
        $scope.usuario.id = null;
    }

    $scope.salvar = function() {
        $scope.usuarioForm.submitted = true;

        if ($scope.usuarioForm.$valid) {
          Usuario.save($scope.usuario, function() {
            flash.success = 'Registro salvo com sucesso!';
            $location.path("/usuario");

          });
        }
    };

    $scope.selectPermissao = function selectPermissao(permissao) {
      var index = $scope.usuario.permissoes.indexOf(permissao);

      /* Se foi selecionado adiciona senao remove */
      if (index > -1) {
        $scope.usuario.permissoes.splice(index, 1);
      }
      else {
        $scope.usuario.permissoes.push(permissao);
      }
    };

  });
