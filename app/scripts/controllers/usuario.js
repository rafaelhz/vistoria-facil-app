'use strict';

/**
 * @ngdoc function
 * @name vistoriaFacilApp.controller:UsuarioCtrl
 * @description
 * # UsuarioCtrl
 * Controller of the vistoriaFacilApp
 */
angular.module('vistoriaFacilWebApp')
  .controller('UsuarioCtrl', function ($scope, Usuario, flash, $http) {

    $scope.nome = "";
    $scope.totalPorPagina = 10;
    $scope.total = 0;
    $scope.paginaAtual = 0;


    $scope.listar = function(paginaAtual, orderBy, orderByReverse) {

      $scope.usuarios = Usuario.get({ page: paginaAtual, limit: $scope.totalPorPagina,
        orderBy: orderBy, orderDirection: ((orderByReverse) ? "DESC" : null), nome: $scope.nome }, function() {
        $scope.total = $scope.usuarios.totalElements;
      });
    };

    $scope.inativar = function(id) {

        Usuario.delete({ id: id }, function() {
            flash.success = 'Usuario inativado com sucesso!';
        });

    };

  });
