'use strict';

/**
 * @ngdoc function
 * @name vistoriaFacilApp.controller:ImobiliariacadastroCtrl
 * @description
 * # ImobiliariacadastroCtrl
 * Controller of the vistoriaFacilApp
 */
angular.module('vistoriaFacilWebApp')
  .controller('ImobiliariaCadastroCtrl', function ($scope, Imobiliaria, Cidade, $routeParams, $location, flash) {

    $scope.inicializar = function(){
        if ($routeParams.id != null) {
            $scope.editar();

        } else {
            $scope.novo();
        }

      $scope.cidades = Cidade.query();
    }



    $scope.editar = function(){
        Imobiliaria.get({ id: $routeParams.id }, function(data) {
            $scope.imobiliaria = data;
         });
    }

    $scope.novo = function(){
        $scope.imobiliaria = {};
        $scope.imobiliaria.id = null;
        $scope.imobiliaria.nome = "";
        $scope.imobiliaria.ativo = true;
    }

    $scope.salvar = function() {

      $scope.imobiliariaForm.submitted = true;

      if ($scope.imobiliariaForm.$valid) {
        console.log($scope.imobiliaria);
        Imobiliaria.save($scope.imobiliaria, function() {
          flash.success = 'Registro salvo com sucesso!';
          $location.path("/imobiliaria");
        });
      }

    };

  });
