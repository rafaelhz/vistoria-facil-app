'use strict';

/**
 * @ngdoc function
 * @name vistoriaFacilWebApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the vistoriaFacilWebApp
 */
angular.module('vistoriaFacilWebApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
