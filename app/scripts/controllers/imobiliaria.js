'use strict';

/**
 * @ngdoc function
 * @name vistoriaFacilApp.controller:ImobiliariaCtrl
 * @description
 * # ImobiliariaCtrl
 * Controller of the vistoriaFacilApp
 */
angular.module('vistoriaFacilWebApp')
  .controller('ImobiliariaCtrl', function ($scope, Imobiliaria, flash, $http) {

    $scope.nome = "";
    $scope.totalPorPagina = 10;
    $scope.total = 0;
    $scope.paginaAtual = 0;

    $scope.listar = function(paginaAtual, orderBy, orderByReverse) {
      $scope.imobiliarias = Imobiliaria.get({ page: paginaAtual, limit: $scope.totalPorPagina,
        orderBy: orderBy, orderDirection: ((orderByReverse) ? "DESC" : null), nome: $scope.nome }, function() {
        $scope.total = $scope.imobiliarias.totalElements;
      });
    };

    $scope.excluir = function(id) {
        Imobiliaria.delete({ id: id }, function() {
            flash.success = 'Registro removido com sucesso!';
        });

    };

  });
