'use strict';

/**
 * @ngdoc function
 * @name vistoriaFacilWebApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the vistoriaFacilWebApp
 */
angular.module('vistoriaFacilWebApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
