'use strict';

/**
 * @ngdoc function
 * @name vistoriaFacilWebApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the vistoriaFacilWebApp
 */
angular.module('vistoriaFacilWebApp')
  .controller('LoginCtrl', function ($scope, $rootScope, $http, $location) {

    $scope.loginUsuario = function() {

      $scope.loginForm.submitted = true;

      if ($scope.loginForm.$valid) {
        sessionStorage.token = btoa($scope.login + ":" + $scope.senha);

        $http.post($SERVER + '/ws/login/',$scope.login)
          .success(function(data,status) {
            sessionStorage.usuario = data;
            $location.path('/');
          })
          .error(function(data, status) {
            alert("Usuário ou senha inválidos.");
          });
      }

    };


  });
